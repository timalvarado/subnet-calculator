from ipaddress import IPv4Address, IPv4Network, AddressValueError

def main():
    ip_address = input('Please enter a network with cidr notation. E.g. 192.168.1.0/24: ')
    ip = ip_address.split('/')[0]
    cidr = ip_address.split('/')[1]
    if validIP(ip)== True:
        num_addresses = getNetworkSize(ip_address)
        num_ips = getNetworkIP(ip_address)
    print('Number of usable IPs is {}.'.format(num_addresses))
    print('The usable IPs is:')
    for ip in num_ips:
        print(ip)

def validIP(ipaddress):
    """
    takes in ipaddress and verifies it is a valid IP
    use try and except for this
    return True if valid, otherwise return False
    """
    try:
        IPv4Address(ipaddress)
        return True
    except AddressValueError:
        return False

def getNetworkSize(ip_network):
    """
    takes in ipaddress and cidr notation
    use try and except for error handling
    returns total number of IPs allowed in subnet
    """
    try:
        num_addresses = IPv4Network(ip_network).num_addresses
        return num_addresses
    except AddressValueError:
        print('Address cannot be empty. Please enter a network like 192.168.1.0/24')

def getNetworkIP(ip_network):
    """
    takes in ipaddress + cidr notation
    use try and except for error handling
    returns a list of all available IPs in the range
    """
    addresses = []
    for addr in IPv4Network(ip_network).hosts():
        addresses.append(addr)
    return addresses

if __name__ == "__main__":
    main()
